from logging import debug
from flask import Flask, request, jsonify
import mysql.connector
from functools import wraps
from mysqlhelper import *

app = Flask(__name__)

@app.route("/buku", methods=['GET'])
def getbarang():
    my = MysqlHelper()
    try:
        result = my.get("buku")
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/buku/<int:id>", methods=['GET'])
def getbarangbyid(id):
    my = MysqlHelper()
    try:
        result = my.get_where("buku", {"id": id}, "row_array")
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/buku", methods=['POST'])
def insertbarang():
    my = MysqlHelper()
    try:
        data = request.json
        if "nama" in data and "penulis" in data and "ringkasan" in data and "tahun_terbit" in data:
            data = { 
                "nama": str(data['nama']),
                "penulis": str(data['penulis']),
                "ringkasan": str(data['ringkasan']),
                "tahun_terbit": int(data['tahun_terbit'])
            }
            result = my.insert("buku", data)
            if result:
                return jsonify({"data":result[1], "code": 200}), 200
            else:
                return jsonify({"data":result[1], "code": 500}), 500
        else:
            return jsonify({"data":"nama, penulis, ringkasan, tahun_terbit", "code": 400}), 400
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/buku/<int:id>", methods=['PUT'])
def updatebarangbyid(id):
    my = MysqlHelper()
    try:
        data = request.json
        if "nama" in data and "penulis" in data and "ringkasan" in data and "tahun_terbit" in data:
            data = {
               "nama": str(data['nama']),
                "penulis": str(data['penulis']),
                "ringkasan": str(data['ringkasan']),
                "tahun_terbit": str(data['tahun_terbit'])
            }
            result = my.update("buku", data, {'id': id})
            if result:
                return jsonify({"data":result[1], "code": 200}), 200
            else:
                return jsonify({"data":result[1], "code": 500}), 500
        else:
             return jsonify({"data":"nama, penulis, ringkasan, tahun_terbit", "code": 400}), 400
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/buku/<int:id>", methods=['DELETE'])
def deletebarangbyid(id):
    my = MysqlHelper()
    try:
        result = my.delete("buku", {"id": id})
        if result:
            return jsonify({"data":result[1], "code": 200}), 200
        else:
            return jsonify({"data":result[1], "code": 500}), 500
            
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

if "__main__" == __name__:
    # app.run(host="0.0.0.0", port=5000)
    # app.run(debug=True, port=1000)
    app.run(debug=True, host="0.0.0.0", port=5000)
