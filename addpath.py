import requests
import json


service = [
    {"name": 'mc-administration', 'url' : 'http://mc.cubetech.id:31000/administration'},
    {"name": 'mc-barang', 'url' : 'http://mc.cubetech.id:31001/barang'},
    {"name": 'mc-buku', 'url' : 'http://mc.cubetech.id:31002/buku'},
    {"name": 'mc-complain', 'url' : 'http://mc.cubetech.id:31003/complain'},
    {"name": 'mc-dashboard', 'url' : 'http://mc.cubetech.id:31004/dashboard'},
    {"name": 'mc-finance', 'url' : 'http://mc.cubetech.id:31005/finance'},
    {"name": 'mc-itmanagement', 'url' : 'http://mc.cubetech.id:31006/itmanagement'},
    {"name": 'mc-pegawai', 'url' : 'http://mc.cubetech.id:31007/pegawai'},
    {"name": 'mc-perjalanandinas', 'url' : 'http://mc.cubetech.id:31008/perjalanandinas'},
    {"name": 'mc-presence', 'url' : 'http://mc.cubetech.id:31009/presence'},
    {"name": 'mc-publicrelation', 'url' : 'http://mc.cubetech.id:31010/publicrelation'},
    {"name": 'mc-reporting', 'url' : 'http://mc.cubetech.id:31011/reporting'},
    {"name": 'mc-task', 'url' : 'http://mc.cubetech.id:31012/task'},
    {"name": 'mc-workflow', 'url' : 'http://mc.cubetech.id:31013/workflow'},
]
inserted = [
    {'name': 'mc-pegawai', 'service': {'id': '2def8d0f-e2b3-47d9-a082-363af314eca1'}, 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/administration'},
    {'name': 'mc-workflow', 'service': {'id': '47ca3fe9-adbc-4eea-9ef8-82e9ea0115e9'}, 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/barang'},
    {'name': 'mc-administration', 'service': {'id': '5a63092f-0234-41a2-a8a8-8752a236055c'}, 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/buku'},
    {'name': 'mc-finance', 'service': {'id': '6712796e-011c-4a5a-96e2-25c24f91ca99'}, 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/complain'},
    {'name': 'mc-itmanagement', 'service': {'id': '72fd425f-ddf3-454a-93c5-0a96eb293eeb'}, 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/dashboard'},
    {'name': 'mc-barang', 'service': {'id': '7ca6fda4-ab89-4e18-8764-23aa580e35be'}, 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/finance'},
    {'name': 'mc-perjalanandinas', 'service': {'id': '9abdc382-c0d2-454b-9b74-db4cd5dc49f7'}, 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/itmanagement'},
    {'name': 'mc-task', 'service': {'id': 'ad9465d9-2a8b-4660-92ab-bbf4c9744877'}, 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/pegawai'},
    {'name': 'mc-publicrelation', 'service': {'id': 'bfc0a2ea-bb50-49b6-8756-c6fb8f889493'}, 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/perjalanandinas'},
    {'name': 'mc-complain', 'service': {'id': 'c4d3689f-7de3-4eea-83f8-53e3d7bf6505'}, 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/presence'},
    {'name': 'mc-buku', 'service': {'id': 'c74248a4-8c5d-4d04-b718-70bb5a7a5bb7'}, 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/publicrelation'},
    {'name': 'mc-presence', 'service': {'id': 'cedb147d-bd14-4f71-810f-bc510b2039ea'}, 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/reporting'},
    {'name': 'mc-reporting', 'service': {'id': 'ef0d169a-0ee4-4b10-bdae-0fd49a8dbc13'}, 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/task'},
    {'name': 'mc-dashboard', 'service': {'id': 'fd61aa07-857e-4622-b1c3-f392e45112d1'}, 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/workflow'},
]
route = [
    {"name": 'mc-administration', 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/administration'},
    # {"name": 'mc-barang', 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/barang'},
    {"name": 'mc-buku', 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/buku'},
    {"name": 'mc-complain', 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/complain'},
    {"name": 'mc-dashboard', 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/dashboard'},
    {"name": 'mc-finance', 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/finance'},
    {"name": 'mc-itmanagement', 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/itmanagement'},
    {"name": 'mc-pegawai', 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/pegawai'},
    {"name": 'mc-perjalanandinas', 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/perjalanandinas'},
    {"name": 'mc-presence', 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/presence'},
    {"name": 'mc-publicrelation', 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/publicrelation'},
    {"name": 'mc-reporting', 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/reporting'},
    {"name": 'mc-task', 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/task'},
    {"name": 'mc-workflow', 'protocols' : 'http', 'methods': ['GET', 'POST', 'PUT', "DELETE"], "hosts": 'mc.cubetech.id', 'paths':'/workflow'},
]
# {"updated_at":1634681891,"headers":null,"protocols":["http"],"service":"mc-finance","name":"mc-finance","request_buffering":true,"response_buffering":true,"path_handling":"v0","tags":null,"https_redirect_status_code":426,"paths":["/finance"],"methods":["GET","POST","PUT","DELETE"],"sources":null,"destinations":null,"created_at":1634681219,"id":"6a154068-5e66-4910-afdf-d80b4cfd91ca","regex_priority":0,"strip_path":true,"hosts":["mc.cubetech.id"],"snis":null,"preserve_host":false}
# for i in service:
#     c = requests.post(url='http://mc.cubetech.id:8001/services', data=i).content
#     print(c)

# c = requests.get(url='http://mc.cubetech.id:8001/services').content.decode('utf-8')
# for i in json.loads(c)['data']:
#     print({"name" : i['name'], "id" : i['id']})

# for i in route:
#     print(i)
#     c = requests.post(url='http://mc.cubetech.id:8001/routes', data=i).content.decode('utf-8')
#     print(c)
for i in inserted:
    print(i)
    c = requests.put(url='http://mc.cubetech.id:8001/routes/'+i['name'], data=i).content.decode('utf-8')
    print(c)

# c = requests.get(url='http://mc.cubetech.id:8001/routes').content.decode('utf-8')
# print(c)
