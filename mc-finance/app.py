from logging import debug
from flask import Flask, request, jsonify
import mysql.connector
from functools import wraps
from mysqlhelper import *

app = Flask(__name__)

@app.route("/finance", methods=['GET'])
def getbarang():
    my = MysqlHelper()
    try:
        result = my.get("finance")
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/finance/<int:id>", methods=['GET'])
def getbarangbyid(id):
    my = MysqlHelper()
    try:
        result = my.get_where("finance", {"id": id}, "row_array")
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/finance", methods=['POST'])
def insertbarang():
    my = MysqlHelper()
    try:
        data = request.json
        if "request_number" in data and "approver" in data and "amount" in data and "description" in data:
            data = { 
                "request_number": str(data['request_number']),
                "approver": str(data['approver']),
                "amount": str(data['amount']),
                "description": str(data['description'])
            }
            result = my.insert("finance", data)
            if result:
                return jsonify({"data":result[1], "code": 200}), 200
            else:
                return jsonify({"data":result[1], "code": 500}), 500
        else:
            return jsonify({"data":"request_number, approver, amount, description", "code": 400}), 400
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/finance/<int:id>", methods=['PUT'])
def updatebarangbyid(id):
    my = MysqlHelper()
    try:
        data = request.json
        if "request_number" in data and "approver" in data and "amount" in data and "description" in data:
            data = {
                "request_number": str(data['request_number']),
                "approver": str(data['approver']),
                "amount": str(data['amount']),
                "description": str(data['description'])
            }
            result = my.update("finance", data, {'id': id})
            if result:
                return jsonify({"data":result[1], "code": 200}), 200
            else:
                return jsonify({"data":result[1], "code": 500}), 500
        else:
             return jsonify({"data":"request_number, approver, amount, description", "code": 400}), 400
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/finance/<int:id>", methods=['DELETE'])
def deletebarangbyid(id):
    my = MysqlHelper()
    try:
        result = my.delete("finance", {"id": id})
        if result:
            return jsonify({"data":result[1], "code": 200}), 200
        else:
            return jsonify({"data":result[1], "code": 500}), 500
            
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

if "__main__" == __name__:
    # app.run(debug=True, host="0.0.0.0", port=5000)
    # app.run(debug=True, port=1000)
    app.run(debug=True, host="0.0.0.0", port=5000)
