from logging import debug
from flask import Flask, request, jsonify
import mysql.connector
from functools import wraps
from mysqlhelper import *

app = Flask(__name__)

@app.route("/public_relation", methods=['GET'])
def getbarang():
    my = MysqlHelper()
    try:
        result = my.get("public_relation")
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/public_relation/<int:id>", methods=['GET'])
def getbarangbyid(id):
    my = MysqlHelper()
    try:
        result = my.get_where("public_relation", {"id": id}, "row_array")
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/public_relation", methods=['POST'])
def insertbarang():
    my = MysqlHelper()
    try:
        data = request.json
        if "title" in data and "media" in data and "status" in data and "content" in data:
            data = { 
                "title": str(data['title']),
                "media": str(data['media']),
                "status": int(data['status']),
                "content": str(data['content'])
            }
            result = my.insert("public_relation", data)
            if result:
                return jsonify({"data":result[1], "code": 200}), 200
            else:
                return jsonify({"data":result[1], "code": 500}), 500
        else:
            return jsonify({"data":"title, media, maker, status, content", "code": 400}), 400
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/public_relation/<int:id>", methods=['PUT'])
def updatebarangbyid(id):
    my = MysqlHelper()
    try:
        data = request.json
        if "title" in data and "media" in data and "status" in data and "content" in data:
            data = {
                "title": str(data['title']),
                "media": str(data['media']),
                "status": int(data['status']),
                "content": str(data['content'])
            }
            result = my.update("public_relation", data, {'id': id})
            if result:
                return jsonify({"data":result[1], "code": 200}), 200
            else:
                return jsonify({"data":result[1], "code": 500}), 500
        else:
             return jsonify({"data":"title, media, maker, status, content", "code": 400}), 400
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/public_relation/<int:id>", methods=['DELETE'])
def deletebarangbyid(id):
    my = MysqlHelper()
    try:
        result = my.delete("public_relation", {"id": id})
        if result:
            return jsonify({"data":result[1], "code": 200}), 200
        else:
            return jsonify({"data":result[1], "code": 500}), 500
            
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

if "__main__" == __name__:
    # app.run(debug=True, host="0.0.0.0", port=5000)
    # app.run(debug=True, port=1000)
    app.run(debug=True, host="0.0.0.0", port=5000)
