from logging import debug
from flask import Flask, request, jsonify
import mysql.connector
from functools import wraps
from mysqlhelper import *

app = Flask(__name__)

@app.route("/workflow", methods=['GET'])
def getbarang():
    my = MysqlHelper()
    try:
        result = my.get("workflow")
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/workflow/<int:id>", methods=['GET'])
def getbarangbyid(id):
    my = MysqlHelper()
    try:
        result = my.get_where("workflow", {"id": id}, "row_array")
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/workflow", methods=['POST'])
def insertbarang():
    my = MysqlHelper()
    try:
        data = request.json
        if "service_type" in data and "service_id" in data and "description" in data:
            data = { 
                "service_type": str(data['service_type']),
                "service_id": int(data['service_id']),
                "description": str(data['description'])
            }
            result = my.insert("workflow", data)
            if result:
                return jsonify({"data":result[1], "code": 200}), 200
            else:
                return jsonify({"data":result[1], "code": 500}), 500
        else:
            return jsonify({"data":"service_type, service_id, description", "code": 400}), 400
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/workflow/<int:id>", methods=['PUT'])
def updatebarangbyid(id):
    my = MysqlHelper()
    try:
        data = request.json
        if "service_type" in data and "service_id" in data and "description" in data:
            data = {
                "service_type": str(data['service_type']),
                "service_id": int(data['service_id']),
                "description": str(data['description'])
            }
            result = my.update("workflow", data, {'id': id})
            if result:
                return jsonify({"data":result[1], "code": 200}), 200
            else:
                return jsonify({"data":result[1], "code": 500}), 500
        else:
             return jsonify({"data":"service_type, service_id, description", "code": 400}), 400
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/workflow/<int:id>", methods=['DELETE'])
def deletebarangbyid(id):
    my = MysqlHelper()
    try:
        result = my.delete("workflow", {"id": id})
        if result:
            return jsonify({"data":result[1], "code": 200}), 200
        else:
            return jsonify({"data":result[1], "code": 500}), 500
            
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

if "__main__" == __name__:
    # app.run(debug=True, host="0.0.0.0", port=5000)
    # app.run(debug=True, port=1000)
    app.run(debug=True, host="0.0.0.0", port=5000)
