from logging import debug
from flask import Flask, request, jsonify
import mysql.connector
from functools import wraps
from mysqlhelper import *

app = Flask(__name__)

@app.route("/pegawai", methods=['GET'])
def getbarang():
    my = MysqlHelper()
    try:
        result = my.get("pegawai")
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/pegawai/<int:id>", methods=['GET'])
def getbarangbyid(id):
    my = MysqlHelper()
    try:
        result = my.get_where("pegawai", {"id": id}, "row_array")
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/pegawai", methods=['POST'])
def insertbarang():
    my = MysqlHelper()
    try:
        data = request.json
        if "nama" in data and "email" in data and "npwp" in data:
            data = {
                "nama": str(data['nama']),
                "email": str(data['email']),
                "npwp": str(data['npwp'])
            }
            result = my.insert("pegawai", data)
            if result == True:
                return jsonify({"data":result[1], "code": 200}), 200
            else:
                return jsonify({"data":result[1], "code": 500}), 500
        else:
            return jsonify({"data":"nama, email, npwp", "code": 400}), 400
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/pegawai/<int:id>", methods=['PUT'])
def updatebarangbyid(id):
    my = MysqlHelper()
    try:
        data = request.json
        if "nama" in data and "email" in data and "npwp" in data:
            data = {
                "nama": str(data['nama']),
                "email": str(data['email']),
                "npwp": int(data['npwp'])
            }
            result = my.update("pegawai", data, {'id': id})
            if result:
                return jsonify({"data":result[1], "code": 200}), 200
            else:
                return jsonify({"data":result[1], "code": 500}), 500
        else:
            return jsonify({"data":"nama, email, npwp", "code": 400}), 400
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/pegawai/<int:id>", methods=['DELETE'])
def deletebarangbyid(id):
    my = MysqlHelper()
    try:
        result = my.delete("pegawai", {"id": id})
        if result:
            return jsonify({"data":result[1], "code": 200}), 200
        else:
            return jsonify({"data":result[1], "code": 500}), 500
            
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

if "__main__" == __name__:
    # app.run(host="0.0.0.0", port=5000)
    # app.run(debug=True, host="0.0.0.0", port=5001)
    # app.run(debug=True, port=5000)
    app.run(debug=True, host="0.0.0.0", port=5000)